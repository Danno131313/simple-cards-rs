//! # Simple Cards
//!
//! A simple deck of cards library.

pub mod cards;
pub mod deck;

pub use crate::cards::Card;
pub use crate::deck::Deck;

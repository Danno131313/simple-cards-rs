use crate::cards::Card;
use crate::cards::Suit::*;
use crate::cards::Value::*;
use rand::{thread_rng, Rng};
use serde_derive::*;
use std::ops::Deref;
use std::slice::{Iter, IterMut};
use std::vec::IntoIter;

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Deck {
    cards: Vec<Card>,
}

impl Default for Deck {
    fn default() -> Self {
        let mut deck = Self::new();
        deck.shuffle();
        deck
    }
}

impl Into<Vec<Card>> for Deck {
    fn into(self) -> Vec<Card> {
        self.cards
    }
}

impl Deck {
    /// Creates a new, ordered deck of 52 cards.
    /// Use `Deck::default()` to get one pre-shuffled.
    pub fn new() -> Deck {
        let mut cards: Vec<Card> = Vec::with_capacity(52);
        let suits = [Hearts, Spades, Diamonds, Clubs];
        let values = [
            Ace, Two, Three, Four, Five, Six, Seven, Eight, Nine, Ten, Jack, Queen, King,
        ];
        for &suit in &suits {
            for &value in &values {
                let card = Card { suit, value };
                cards.push(card);
            }
        }
        Deck { cards }
    }

    /// Creates a new, empty deck.
    pub fn new_empty() -> Deck {
        let cards: Vec<Card> = Vec::with_capacity(52);
        Deck { cards }
    }

    /// Clears the deck of all cards.
    pub fn clear(&mut self) {
        self.cards.clear();
    }

    /// Returns the amount of cards in the deck.
    pub fn len(&self) -> usize {
        self.cards.len()
    }

    /// Returns whether or not the deck is empty.
    pub fn is_empty(&self) -> bool {
        self.cards.is_empty()
    }

    /// Returns reference to a card given an index.
    pub fn show(&self, idx: usize) -> Option<&Card> {
        self.cards.get(idx)
    }

    /// Adds a card to the bottom of the deck.
    pub fn add(&mut self, card: Card) {
        self.cards.push(card);
    }

    /// Adds a card to the front of the deck.
    pub fn add_bottom(&mut self, card: Card) {
        self.cards.insert(0, card);
    }

    /// Adds a deck of cards, one by one, to the bottom of this deck.
    pub fn add_deck(&mut self, deck: &mut Deck) {
        while !deck.is_empty() {
            self.cards.push(deck.draw().unwrap());
        }
    }

    /// Draws one card from the front of the deck, returning an Option<Card>.
    pub fn draw(&mut self) -> Option<Card> {
        match self.cards.len() {
            0 => None,
            _ => Some(self.cards.remove(0)),
        }
    }

    /// Draws one card from the bottom of the deck, return an Option<Card>.
    pub fn draw_bottom(&mut self) -> Option<Card> {
        match self.cards.len() {
            0 => None,
            _x => Some(self.cards.remove(_x - 1)),
        }
    }

    /// Shuffles the deck.
    pub fn shuffle(&mut self) {
        let mut rng = thread_rng();
        rng.shuffle(&mut self.cards);
    }

    /// Drains deck into another deck
    pub fn drain_into(&mut self, deck: &mut Deck) {
        deck.add_deck(self);
    }
}

impl IntoIterator for Deck {
    type Item = Card;
    type IntoIter = IntoIter<Card>;

    fn into_iter(self) -> Self::IntoIter {
        self.cards.into_iter()
    }
}

impl<'a> IntoIterator for &'a Deck {
    type Item = &'a Card;
    type IntoIter = Iter<'a, Card>;

    fn into_iter(self) -> Self::IntoIter {
        self.cards.iter()
    }
}

impl<'a> IntoIterator for &'a mut Deck {
    type Item = &'a mut Card;
    type IntoIter = IterMut<'a, Card>;

    fn into_iter(self) -> Self::IntoIter {
        self.cards.iter_mut()
    }
}

impl Deref for Deck {
    type Target = [Card];

    fn deref(&self) -> &Self::Target {
        &self.cards
    }
}
